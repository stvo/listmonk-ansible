# Ansible Playbook for a listmonk host

This is an ansible role for setting up a listmonk server.
The role is based on the installation instructions of https://listmonk.app/docs/installation/ and is not using docker.

I put this together in about 30 mins and tested it once. So please don't expect it to be perfect :)
